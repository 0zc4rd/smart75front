export interface Empleados {
    id: number;
    nombre: string;
    correo: string;
    telefono: string;
    rol: string;
}
