// TODO: Replace this with your own data model type
export interface Empresas {
    id: number;
    nit: string;
    nombre: string;
    correo: string;
    telefono: string;
    direccion: string;
    contacto: string;
    ciudad: string;
  }