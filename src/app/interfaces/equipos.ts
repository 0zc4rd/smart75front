// TODO: Replace this with your own data model type
export interface Equipos {
    id: number;
    tipo: string;
    marca: string;
    serial: string;
    modelo: string;
    cliente: string;
    observaciones: string;
  }
  