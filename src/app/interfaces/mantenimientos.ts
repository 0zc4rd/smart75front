
// TODO: Replace this with your own data model type
export interface Mantenimientos {
    id: number;
    costo: number;
    ingreso: string;
    salida: string;
    estado: boolean;
    equipo: string;
    tecnico: string;
    observaciones: string;
  }
  